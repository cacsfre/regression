FROM rocker/r-rmd
MAINTAINER Carlos C. Sepulveda "cachs1@ulaval.ca"

# OS dependencies
RUN apt-get update --allow-releaseinfo-change && apt-get install -y libcurl4-openssl-dev

# R packages
RUN R -e "install.packages(c('learnr', 'withr'))"
RUN R -e "install.packages(c('quantmod', 'ISLR2'))"

