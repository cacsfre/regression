# Atelier interactif

Cet atelier vous permet d'exécuter des commandes `R` directement sur un navigateur web. 

> Créé avec `R` ([learnr](https://rstudio.github.io/learnr/)).

## Avec RStudio

Il suffit d'ouvrir le fichier `index.Rmd` avec RStudio et de cliquer sur le bouton `Run Document`.

## Avec Docker

D'abord, vous aurez besoin d'installer `docker` sur votre système d'exploitation ([instructions](https://docs.docker.com/engine/install/)). Ensuite, il suffit d'utiliser les commandes `make` disponibles pour créer l'image et lancer un conteneur. Toute dépendance OS ou R s'y trouvera.

1. Créer et lancer une image:

```bash
make container_build  # créer l'image
make container_attach  # lancer un conteneur
```

2. Lancer l'atelier:

```bash
make run  # à partir du dossier fr
```
