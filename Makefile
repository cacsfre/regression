container_build:
	docker build -t regression/r-learnr .

container_attach:
	docker run --rm --network host -u 1000:1000 -it -v ./:/regression -w /regression regression/r-learnr

